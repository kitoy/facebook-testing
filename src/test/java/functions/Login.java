package functions;

import static pom.Login_POM.*;

public class Login {
    public static void SetUsername(String username) {
        Common.InputText(username(), username);
    }

    public static void SetPassword(String password) {
        Common.InputText(password(), password);
    }

    public static void ClickUsernameNext() {
        usernameNext().click();
    }

    public static void ClickPasswordNext() {
        passwordNext().click();
    }
}
