package functions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import test.BaseTest;

public class Overview extends BaseTest {
    public static WebElement inbox() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@title='Inbox']")));
    }
}
