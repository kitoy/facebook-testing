package pom;

import org.apache.xalan.xsltc.dom.AdaptiveResultTreeImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.webbitserver.WebbitException;
import test.BaseTest;

public class Login_POM extends BaseTest {

    public static WebElement username() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='email']")));
    }

    public static WebElement password() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("password")));
    }

    public static WebElement login() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='submit']")));
    }

    public static WebElement usernameNext() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("identifierNext")));
    }

    public static WebElement passwordNext() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordNext")));
    }
}
