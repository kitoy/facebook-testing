package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.Driver;

import static util.Driver.CreateDriver;
import static util.Driver.CreateWait;

public class BaseTest {
    public WebDriver driver = null;
    public static WebDriverWait wait = null;

    public void OpenURL(String url) {
        driver = CreateDriver();
        wait = CreateWait(driver);
        driver.manage().window().maximize();
        driver.get(url);
    }
}
