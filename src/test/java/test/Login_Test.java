package test;

import functions.Login;
import org.testng.Assert;
import org.testng.annotations.Test;

import static functions.Login.*;
import static functions.Overview.*;

public class Login_Test extends BaseTest {

    @Test
    public void LoginToGmail() throws InterruptedException {
        String url = "https://www.gmail.com";
        String username = "keith.test.selenium@gmail.com";
        String password = "Pass1234!";

        OpenURL(url);
        SetUsername(username);
        ClickUsernameNext();
        SetPassword(password);
        ClickPasswordNext();

        // validate if inbox appears
        Assert.assertEquals(inbox().getText(), "Inbox");
        driver.quit();
    }
}
