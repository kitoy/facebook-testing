package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Driver {
    public static WebDriver CreateDriver() {
        //System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ "\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        return driver;
    }

    public static WebDriverWait CreateWait(WebDriver driver) {
        return new WebDriverWait(driver, 10);
    }
}
